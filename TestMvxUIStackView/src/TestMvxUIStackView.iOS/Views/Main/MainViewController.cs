using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using TestMvxUIStackView.Core.ViewModels;
using TestMvxUIStackView.iOS.Views.Base;
using UIKit;

namespace TestMvxUIStackView.iOS.Views.Main
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public class MainViewController : BaseViewController<MainViewModel>
    {
        private UIButton _autoButton, _manualButton;

        protected override void CreateView()
        {
            _autoButton = new UIButton(UIButtonType.RoundedRect);
            _autoButton.SetTitle("Auto button", UIControlState.Normal);
            Add(_autoButton);

            _manualButton = new UIButton(UIButtonType.RoundedRect);
            _manualButton.SetTitle("Manual button", UIControlState.Normal);
            Add(_manualButton);
        }

        protected override void LayoutView()
        {
            View.AddConstraints(new FluentLayout[]
           {
               _autoButton.WithSameCenterX(View),
               _autoButton.WithSameCenterY(View),
               _autoButton.WithSameWidth(View),

               _manualButton.Below(_autoButton, 10f),
               _manualButton.WithSameWidth(View)
           });
        }

        protected override void BindView()
        {
            var bindingSet = this.CreateBindingSet<MainViewController, MainViewModel>();
            bindingSet.Bind(_autoButton).To(vm => vm.GoToAutoPage);
            bindingSet.Bind(_manualButton).To(vm => vm.GoToManualPage);
            bindingSet.Apply();
        }
    }
}
