using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using TestMvxUIStackView.Core.ViewModels;
using TestMvxUIStackView.iOS.Views.Base;
using TestMvxUIStackView.iOS.Views.Manual;
using UIKit;

namespace MvvmCross.StackView.Sample.iOS.Views
{
    public class ManualView : BaseViewController<ManualViewModel>
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var stackView = new ManualStackView()
            {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Distribution = UIStackViewDistribution.FillProportionally,
                Alignment = UIStackViewAlignment.Fill,
                Axis = UILayoutConstraintAxis.Vertical,
                Spacing = 5
            };

            Add(stackView);

            View.AddConstraints(stackView.WithSameTop(View),
                                                  stackView.WithSameRight(View),
                                                  stackView.WithSameLeft(View));

            var set = this.CreateBindingSet<ManualView, ManualViewModel>();
            set.Bind(stackView).For(view => view.ItemsSource).To(vm => vm.ListItems);
            set.Apply();
        }
    }
}
