using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.StackView;
using MvvmCross.ViewModels;
using TestMvxUIStackView.Core.ViewModels;
using UIKit;

namespace TestMvxUIStackView.iOS.Views.Manual
{
    public class ManualStackView : MvxStackView
    {
        protected override UIView GetView(MvxViewModel viewModel, int index)
        {
            return new ManualListItemView
            {
                DataContext = viewModel
            };
        }

        private sealed class ManualListItemView : MvxView
        {
            public ManualListItemView()
            {
                BackgroundColor = UIColor.Yellow;
                HeightAnchor.ConstraintGreaterThanOrEqualTo(60).Active = true;

                var titleLabel = new UILabel
                {
                    Font = UIFont.SystemFontOfSize(17),
                    TranslatesAutoresizingMaskIntoConstraints = false
                };

                AddSubview(titleLabel);

                titleLabel.CenterYAnchor.ConstraintEqualTo(CenterYAnchor).Active = true;
                titleLabel.LeftAnchor.ConstraintEqualTo(LeftAnchor, 16).Active = true;

                var subtitleLabel = new UILabel
                {
                    Font = UIFont.SystemFontOfSize(12),
                    TranslatesAutoresizingMaskIntoConstraints = false
                };

                AddSubview(subtitleLabel);

                subtitleLabel.LeftAnchor.ConstraintEqualTo(titleLabel.LeftAnchor).Active = true;
                subtitleLabel.TopAnchor.ConstraintEqualTo(titleLabel.BottomAnchor, 6).Active = true;

                this.DelayBind(() =>
                {
                    var set = this.CreateBindingSet<ManualListItemView, ManualListItem>();
                    set.Bind(titleLabel).To(vm => vm.Title);
                    set.Bind(subtitleLabel).To(vm => vm.Subtitle);
                    set.Apply();
                });
            }
        }
    }
}
