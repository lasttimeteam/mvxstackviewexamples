using MvvmCross.Platforms.Ios.Views;
using MvvmCross.ViewModels;

namespace TestMvxUIStackView.iOS.Views._Base
{
    public class BaseItemViewController<TViewModel> : MvxViewController<TViewModel>
        where TViewModel : class, IMvxViewModel
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            CreateView();

            LayoutView();

            BindView();
        }

        protected virtual void CreateView()
        {
        }

        protected virtual void LayoutView()
        {
        }

        protected virtual void BindView()
        {
        }
    }
}
