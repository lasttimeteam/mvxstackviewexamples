using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.StackView;
using TestMvxUIStackView.Core.ViewModels;
using TestMvxUIStackView.iOS.Views.Base;
using UIKit;

namespace TestMvxUIStackView.iOS.Views.Auto
{
    public class AutoView : BaseViewController<AutoViewModel>
    {
        private MvxStackView _stackView;

        protected override void CreateView()
        {
            _stackView = new MvxStackView
            {
                Distribution = UIStackViewDistribution.FillProportionally,
                Axis = UILayoutConstraintAxis.Vertical,
                Alignment = UIStackViewAlignment.Fill,
                Spacing = 5
            };

            View.Add(_stackView);
        }

        protected override void LayoutView() =>
            View.AddConstraints(_stackView.AtTopOf(View),
                _stackView.AtLeftOf(View),
                _stackView.AtRightOf(View),
                _stackView.AtBottomOf(View));

        protected override void BindView() =>
            this.CreateBindingSet<AutoView, AutoViewModel>()
                .Bind(_stackView)
                .For(view => view.ItemsSource)
                .To(vm => vm.ListItems)
                .Apply();
    }
}
