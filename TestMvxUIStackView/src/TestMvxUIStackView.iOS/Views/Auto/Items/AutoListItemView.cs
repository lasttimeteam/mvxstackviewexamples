using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using TestMvxUIStackView.Core.ViewModels;
using TestMvxUIStackView.iOS.Views._Base;
using UIKit;

namespace TestMvxUIStackView.iOS.Views.Auto.Items
{
    public class AutoListItemView : BaseItemViewController<AutoListItem>
    {
        private UILabel _titleLabel;
        private UILabel _subtitleLabel;

        //Doesn't work without this default constructor
        public AutoListItemView() {}

        protected override void CreateView()
        {
            View.BackgroundColor = UIColor.Yellow;

            _titleLabel = new UILabel
            {
                Font = UIFont.SystemFontOfSize(17),
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            View.Add(_titleLabel);
            
            _subtitleLabel = new UILabel
            {
                Font = UIFont.SystemFontOfSize(12),
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            View.Add(_subtitleLabel);
        }

        protected override void LayoutView()
        {
            _titleLabel.CenterYAnchor.ConstraintEqualTo(View.CenterYAnchor).Active = true;
            _titleLabel.LeftAnchor.ConstraintEqualTo(View.LeftAnchor, 16).Active = true;


            _subtitleLabel.LeftAnchor.ConstraintEqualTo(_titleLabel.LeftAnchor).Active = true;
            _subtitleLabel.TopAnchor.ConstraintEqualTo(_titleLabel.BottomAnchor, 6).Active = true;
        }

        protected override void BindView()
        {
            var set = this.CreateBindingSet<AutoListItemView, AutoListItem>();
            set.Bind(_titleLabel).To(vm => vm.Title);
            set.Bind(_subtitleLabel).To(vm => vm.Subtitle);
            set.Apply();
        }
    }
}
