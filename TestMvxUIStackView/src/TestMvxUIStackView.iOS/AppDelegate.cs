﻿using Foundation;
using MvvmCross.Platforms.Ios.Core;
using TestMvxUIStackView.Core;

namespace TestMvxUIStackView.iOS
{
    [Register(nameof(AppDelegate))]
    public class AppDelegate : MvxApplicationDelegate<Setup, App>
    {
    }
}
