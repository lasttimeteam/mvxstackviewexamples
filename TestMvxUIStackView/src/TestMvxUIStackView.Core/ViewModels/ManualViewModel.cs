using System.Threading.Tasks;
using MvvmCross.ViewModels;

namespace TestMvxUIStackView.Core.ViewModels
{
    public class ManualViewModel : BaseViewModel
    {
        public MvxObservableCollection<ManualListItem> ListItems { get; }

        public ManualViewModel()
        {
            ListItems = new MvxObservableCollection<ManualListItem>();
        }

        public override  Task Initialize()
        {
            for (var index = 0; index < 5; index++)
            {
                var listItem = new ManualListItem
                {
                    Title = $"Title {index}",
                    Subtitle = $"Subtitle {index}"
                };

                ListItems.Add(listItem);
            }

            return Task.CompletedTask;
        }
    }
}
