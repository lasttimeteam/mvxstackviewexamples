using MvvmCross;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace TestMvxUIStackView.Core.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
        protected BaseViewModel()
        {
            NavigationService = Mvx.IoCProvider.Resolve<IMvxNavigationService>();
        }
        protected IMvxNavigationService NavigationService { get; }
    }
}
