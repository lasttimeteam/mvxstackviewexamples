using System.Threading.Tasks;
using MvvmCross.ViewModels;

namespace TestMvxUIStackView.Core.ViewModels
{
    public class AutoViewModel : MvxViewModel
    {
        public AutoViewModel()
        {
            ListItems = new MvxObservableCollection<AutoListItem>();
        }
        public MvxObservableCollection<AutoListItem> ListItems { get; }
        
        public override Task Initialize()
        {
            for (var index = 0; index < 10; index++)
            {
                var listItem = new AutoListItem
                {
                    Title = $"Title {index}",
                    Subtitle = $"Subtitle {index}"
                };

                ListItems.Add(listItem);
            }

            return base.Initialize();
        }
    }
}
