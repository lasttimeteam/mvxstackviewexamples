using MvvmCross.ViewModels;

namespace TestMvxUIStackView.Core.ViewModels
{
    public class ManualListItem : MvxViewModel
    {
        public string Title { get; set; }

        public string Subtitle { get; set; }
    }
}
