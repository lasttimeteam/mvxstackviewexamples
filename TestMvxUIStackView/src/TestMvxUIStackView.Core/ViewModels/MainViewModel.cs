using MvvmCross.Commands;

namespace TestMvxUIStackView.Core.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public MvxCommand GoToAutoPage => new MvxCommand(() => NavigationService.Navigate<AutoViewModel>());

        public MvxCommand GoToManualPage => new MvxCommand(() => NavigationService.Navigate<ManualViewModel>());
    }
}
