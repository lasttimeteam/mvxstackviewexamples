using MvvmCross.IoC;
using MvvmCross.ViewModels;
using TestMvxUIStackView.Core.ViewModels;

namespace TestMvxUIStackView.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<MainViewModel>();
        }
    }
}
